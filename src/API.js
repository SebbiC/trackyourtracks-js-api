var API = {

  url: "https://trackyourtracks.eu-gb.mybluemix.net/api",

  createCommunity: function(community, callback) {
    return API.createRequest("POST", API.url + "/communities", null, null, null, community, callback);
  },

  getCommunity: function(community, callback) {
    return API.createRequest("GET", API.url + "/communities/" + community.community_name, null, community.user_name, community.user_password, null, callback);
  },

  deleteCommunity: function(community, callback) {
    return API.createRequest("DELETE", API.url + "/communities/" + community.community_name, null, community.user_name, community.user_password, null, callback);
  },

  createUser: function(token, user, callback) {
    return API.createRequest("POST", API.url + "/users", token.community_token, null, null, user, callback);
  },

  updateUser: function(token, creds, user, callback) {
    return API.createRequest("PUT", API.url + "/users/" + creds.user_name, token.community_token, creds.user_name, creds.user_password, user, callback);
  },

  getUser: function(token, creds, callback) {
    return API.createRequest("GET", API.url + "/users/" + creds.user_name, token.community_token, creds.user_name, creds.user_password, null, callback);
  },

  deleteUser: function(token, creds, callback) {
    return API.createRequest("DELETE", API.url + "/users/" + creds.user_name, token.community_token, creds.user_name, creds.user_password, null, callback);
  },

  createTrack: function(token, creds, track, callback) {
    return API.createRequest("POST", API.url + "/tracks", token.community_token, creds.user_name, creds.user_password, track, callback);
  },

  updateTrack: function(token, creds, track_name, track, callback) {
    return API.createRequest("PUT", API.url + "/tracks/" + track_name, token.community_token, creds.user_name, creds.user_password, track, callback);
  },

  getTrack: function(token, creds, track_name, callback) {
    return API.createRequest("GET", API.url + "/tracks/" + track_name, token.community_token, creds.user_name, creds.user_password, null, callback);
  },

  deleteTrack: function(token, creds, track_name, callback) {
    return API.createRequest("DELETE", API.url + "/tracks/" + track_name, token.community_token, creds.user_name, creds.user_password, null, callback);
  },

  createRequest: function(method, url, community_token, user_name, user_password, entity, callback) {

    var request = new Request(method, url, community_token, user_name, user_password, entity, function(response) {
      callback(response);
    });

    request.send();
    return request;
  }


};
