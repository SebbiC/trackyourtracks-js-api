function Response(code, entity) {

  this.code = code;
  this.entity = entity === "" ? "" : JSON.parse(entity);

  this.isOk = function() {
    return code >= 200 && code < 300;
  };

  this.isError = function() {
    return code >= 400 && code < 600;
  };

}
