function Request(method, url, community_token, user_name, user_password, entity, callback) {

  this.method = method;
  this.url = url;
  this.community_token = community_token;
  this.user_name = user_name;
  this.user_password = user_password;
  this.entity = entity;
  this.callback = callback;

  this.send = function() {

    var xhr = new XMLHttpRequest();
    xhr.open(method, url);

    if (community_token !== null) {
      xhr.setRequestHeader("Token", community_token);
    }

    if (user_name !== null && user_password !== null) {
      var base64 = btoa(user_name + ":" + user_password);
      xhr.setRequestHeader("Authorization", "Basic " + base64);
    }

    xhr.onload = function() {
      callback(new Response(xhr.status, xhr.responseText));
    };

    if (entity === null) {
      xhr.send();
    } else {
      xhr.setRequestHeader("Content-type", "application/json");
      xhr.send(JSON.stringify(entity));
    }

  };

}
