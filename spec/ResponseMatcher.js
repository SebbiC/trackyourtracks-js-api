beforeEach(function() {
  jasmine.addMatchers({
    toBeOk: function() {
      return {
        compare: function(actual, expected) {
          var result = {};
          result.pass = actual.isOk();
          result.message = "response code was " + actual.code;
          return result;
        }
      };
    }
  });
});
