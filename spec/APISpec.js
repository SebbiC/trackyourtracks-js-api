jasmine.DEFAULT_TIMEOUT_INTERVAL = 5000;

describe("TYT JS API", function() {

  // SPEC OBJECTS

  var community_1 = new Community("test_community_name_1", "test_user_name_1", "test_user_password_1");
  var community_2 = new Community("test_community_name_2", "test_user_name_2", "test_user_password_2");

  var token_1;
  var token_2;

  var user_1 = new User("test_name_1", "test_password_1", "test_mail_1");
  var user_2 = new User("test_name_2", "test_password_2", "test_mail_2");
  var user_3 = new User("test_name_3", "test_password_3", "test_mail_3");

  var creds_1 = new Creds("test_name_1", "test_password_1");
  var creds_2 = new Creds("test_name_2", "test_password_2");
  var creds_3 = new Creds("test_name_3", "test_password_3");

  var track_1 = new Track("test_name_1", "test_description_1", ["test_keyword_1"], "test_start_1", "test_end_1", 1);
  var track_2 = new Track("test_name_2", "test_description_2", ["test_keyword_2"], "test_start_2", "test_end_2", 2);

  // SPECS

  it("creates a community", function(done) {
    API.createCommunity(community_1, function(response) {
      token_1 = new Token(response.entity.token);
      expect(response).toBeOk();
      done();
    });
  });

  // TODO -- updates a community

  it("gets a community", function(done) {
    API.getCommunity(community_1, function(response) {
      token_1 = new Token(response.entity.token);
      expect(response).toBeOk();
      done();
    });
  });

  it("creates a user", function(done) {
    API.createUser(token_1, user_1, function(response) {
      expect(response).toBeOk();
      done();
    });
  });

  it("updates a user", function(done) {
    API.updateUser(token_1, creds_1, user_2, function(response) {
      expect(response).toBeOk();
      done();
    });
  });

  it("gets a user", function(done) {
    API.getUser(token_1, creds_2, function(response) {
      expect(response).toBeOk();
      done();
    });
  });

  it("creates a track", function(done) {
    API.createTrack(token_1, creds_2, track_1, function(response) {
      expect(response).toBeOk();
      done();
    });
  });

  // it("updates a track", function(done) {
  //   API.updateTrack(token_1, creds_2, track_1.track_name, track_2, function(response) {
  //     expect(response).toBeOk();
  //     done();
  //   });
  // });

  // TODO -- creates a second user

  // TODO -- adds a track to second user

  // TODO -- removes a track from second user

  it("deletes a track", function(done) {
    API.deleteTrack(token_1, creds_2, track_1.track_name, function(response) {
      expect(response).toBeOk();
      done();
    });
  });

  it("deletes a user", function(done) {
    API.deleteUser(token_1, creds_2, function(response) {
      expect(response).toBeOk();
      done();
    });
  });

  it("deletes a community", function(done) {
    API.deleteCommunity(community_1, function(response) {
      expect(response).toBeOk();
      done();
    });
  });

});
